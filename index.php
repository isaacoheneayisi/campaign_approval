
<?php
include_once ('includes/config.php');
include_once ('includes/func.php');
$page_title = "Home";
$section    = "All Campaigns";
//$success_sms = get_total_success();
//$all_sms = get_total_sms();
//$sms_fail =get_total_fail();
//$success_rate =0;
//if($all_sms > 0)$success_rate = round(($success_sms/$all_sms)* 100);

include ("header.php");

?>
<script type="text/javascript">
    // setTimeout(function () {
    //   location.reload();
    // }, 60 * 1000);
    //var client_id = $("#client_id").val();
    //$("client_idd").val(client_id);


</script>

<style type="text/css">

    .img-circle {
    border-radius: 50%;
}

</style>

<script type="text/javascript">
    

</script>
        <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">

<?php include_once ('includes/breadcrumbs/breadcrumbs.php');?>
<div class="clearfix"></div>
                    <!-- END DASHBOARD STATS 1-->
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-globe"></i>All Campaigns </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body" >
                                    <div id="view_campaign_table" >  </div>

                                </div>                                  
                               
                            </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->


 <div class="modal fade" id="camp_details" tabindex="-1" role="dialog" aria-labelledby="camp_details">
        <div class="vertical-alignment-helper">
            <form id="add_clients" autocomplete="false" class="form-horizontal">
                <input type="hidden" name="opera" id="opera" value="add_clients"/>
            <div class="modal-dialog modal-sm vertical-align-center" role="document">
                <div class="modal-content" style="width: 800px">
                    <div class="row" style="margin-left:0; margin-right:0; padding-bottom:10px">


                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <h3 style="margin:10px 0 10px 0;" class="text-center">Campaign Details</h3>
                                <!--                                <img src="images/visamaster.png" width="100%">-->

                            </div>

                            <div class="modal-body">
                               <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <div style="margin-left: 5px;" class="form-group">
                                    <label> ID</label>
                                    <input required="" class="form-control addID" style="width:100%;" type="text" id="camp_id" name="camp_id"  value="" placeholder=""  readonly="" ="" />
                                   
                                </div>

                            </div>
                               <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <div style="margin-left: 5px;" class="form-group">
                                    <label>Owerner ID</label>
                                    <input required="" class="form-control ownerID" style="width:100%;" type="text" id="ownerid" name="ownerid" value="" placeholder="" readonly="" />
                                    
                                </div>

                            </div>
                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <div style="margin-left: 5px;" class="form-group">
                                    <label>Country</label>
                                    <input required="" class="form-control country" style="width:100%;" type="text" id="country" name="country" value="" placeholder="" readonly="" />
                                   
                                </div>

                            </div>


                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <div style="margin-left: 5px;" class="form-group">
                                    <label>Title</label>
                                    <input required="required" class="form-control title" style="width:100%;" type="text" id="title" name="title" value="" readonly=""/>
                                </div>

                            </div>

                             <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <div style="margin-left: 5px;" class="form-group">
                                    <label>Date Created</label>
                                    <input required="required" class="form-control datecreated" style="width:100%;" type="text" id="datecreated" name="datecreated" value="" readonly=""/>
                                </div>

                            </div>


                             <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <div style="margin-left: 5px;" class="form-group">
                                    <label>Date Ended</label>
                                    <input required="required" class="form-control dateend" style="width:100%;" type="text" id="dateend" name="dateend" value="" placeholder="" readonly=""/>
                                </div>

                            </div> 


                             <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <div style="margin-left: 5px;" class="form-group">
                                    <label>Amount Goal</label>
                                    <input required="required" class="form-control goal" style="width:100%;" type="text" id="goal" name="goal" value="" placeholder="" readonly="" />
                                </div>

                            </div> 

                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <div style="margin-left: 5px;" class="form-group">
                                    <label>Amount Achieved</label>
                                    <input required="required" class="form-control achieve" style="width:100%;" type="text" id="achieve" name="achieve" value="" placeholder="" readonly=""/>
                                </div>

                            </div> 


                           <!--  <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <div style="margin-left: 5px;" class="form-group">
                                    <label>Brief</label>
                                    <textarea class="form-control brief" rows="5" id="comment" readonly=""></textarea>

                                </div>

                            </div>
 -->
                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <div style="margin-left: 5px;" class="form-group">
                                    <label>Publish_Status</label>
                                   <input required="required" class="form-control status" style="width:100%;" type="text" id="status" name="status" value="" placeholder="" readonly=""/>

                                </div>

                            </div>


                            <br/>
                        </div>
                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <button style="width:100%;" type="button" class="btn btn-primary btn-lg " data-toggle="modal" data-target="#change_pub_status"><i class="fa fa-credit-card fa-2"></i> Publish/Unpublish</button>
                                <!--                                <img src="images/visamaster.png" width="100%">-->
                            </div>

                        </form>


                    </div>
                </div>
            </div>
        </form>
        </div>
    </div>


<div class="modal fade" id="change_pub_status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Confirmation
            </div>
            <div class="modal-body">
                 Do you really want to change the publication status
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger btn-ok">Proceed</a>
            </div>
        </div>
    </div>
</div>
 <!-- <p>Content here. <a class="alert" href=#>Alert!</a></p> -->

 <!-- modal for account credit  -->



<!--
 <script type="text/javascript">
            $(function () {
                $('#datetimepicker4').datetimepicker();
            });
            $(function () {
                $('#datetimepicker44').datetimepicker();
            });

        </script> -->

<?php
include ("footer.php");
?>

                      <?php   
					  
					        include ("header.php");
					   ?>
                        
                          <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    
                    <?php include_once('includes/breadcrumbs/breadcrumbs.php'); ?>
                 
                    <div class="clearfix"></div>
                    <!-- END DASHBOARD STATS 1-->
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-globe"></i>All Contents </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th> Username</th>
                                                <th> FullName</th>
                                                <th> Permission</th>
                                                <th> Area Privilleges</th>   
                                                <th> Status </th> 
                                                																								
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td> Admin </td>
                                                <td> Main Admin </td>
                                                <td> View,Add,Update, </td>
                                                <td> Support </td>
                                                <td> Active</td>
												
                                            </tr>
                                             <tr>
                                                <td> Admin </td>
                                                <td> Main Admin </td>
                                                <td> View,Add,Update, </td>
                                                <td> Support </td>
                                                <td> Active</td>											
                                            </tr>
											
											 <tr>
                                                <td> Admin </td>
                                                <td> Main Admin </td>
                                                <td> View,Add,Update, </td>
                                                <td> Support </td>
                                                <td> Active</td>
												
                                            </tr>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                      
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
      
	                      <?php   
					  
					        include ("footer.php");
					   ?>
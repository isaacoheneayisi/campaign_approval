
<?php
include_once('includes/config.php');
include_once('includes/func.php');
$page_title = "Organization";
$section = "Organization Accounts"; 
//$success_sms = get_total_success();
//$all_sms = get_total_sms();
//$sms_fail =get_total_fail();
//$success_rate =0;
//if($all_sms > 0)$success_rate = round(($success_sms/$all_sms)* 100); 

include ("header.php"); 



?>
<script type="text/javascript">
    // setTimeout(function () { 
    //   location.reload();
    // }, 60 * 1000);
   // var client_id = $("#client_id").val();
    //$("client_idd").val(client_id);


</script>

<style type="text/css">
    
    .img-circle {
    border-radius: 50%;
}
.error {
    color: red;
}

</style>
        <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            
                    <?php include_once('includes/breadcrumbs/breadcrumbs.php'); ?>
          
                    <div class="clearfix"></div>
                    <!-- END DASHBOARD STATS 1-->
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-globe"></i>Personal Registration</div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                     <div id = "view_organization_table"> </div>
                               </div>
                            </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->

 
<div class="modal fade" id="userDoc" tabindex="-1" role="dialog" aria-labelledby="camp_details">
        <div class="vertical-alignment-helper">
            <form id="verified_form" autocomplete="false" class="form-horizontal">
                <input type="hidden" name="opera" id="opera" value="verified_acc"/>
            <div class="modal-dialog modal-sm vertical-align-center" role="document">
                <div class="modal-content" style="width: 800px">
                    <div class="row" style="margin-left:0; margin-right:0; padding-bottom:10px">


                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <h3 style="margin:10px 0 10px 0;" class="text-center">View Documents</h3>
                                <!--                                <img src="images/visamaster.png" width="100%">-->

                            </div>

                            <div class="modal-body">
                               <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <div style="margin-left: 5px;" class="form-group">
                                    <label> ID</label>
                                    <input required="" class="form-control addID" style="width:100%;" type="text" id="camp_id" name="camp_id"  value="" placeholder=""  readonly="" />
                                   
                                </div>

                            </div>
                           
                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <div style="margin-left: 5px;" class="form-group">
                                    <label> Email</label>
                                    <input required="" class="form-control email_user" style="width:100%;" type="text" id="email" name="email"  value="" placeholder=""  readonly="" />
                                   
                                </div>

                            </div>

                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                    <div >
                                           <label>Director ID</label>
                                      <iframe class="embed-responsive-item" id="iframe" width="100%" height="500px" ></iframe>
                                    </div>
                                    
                               </div>
                               <div class="col-md-12 col-xs-12 col-sm-12 col-lsg-12">

                                    <div >
                                         <label>Business Document</label>
                                      <iframe class="embed-responsive-item" id="iframe_doc" width="100%" height="500px" ></iframe>
                                    </div>
                                    
                               </div>
                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                <div class="row">
                                    <div class="col-md-6">
                                         <button id="verified" style="width:100%;" type="button" class="btn btn-primary btn-lg " ><i class="fa fa-credit-card fa-2"></i> Verified </button>
                                    </div>
                                    <div class="col-md-6">
                                          <button style="width:100%;" type="button" class="btn btn-danger btn-lg" id="unconfirmed"><i class="fa fa-credit-card fa-2"></i> Unverified</button>
                                    </div>
                                </div>
                               
                                <!--                                <img src="images/visamaster.png" width="100%">-->
                            </div>
                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                               
                                <!--                                <img src="images/visamaster.png" width="100%">-->
                            </div>

                        </form>


                    </div>
                </div>
            </div>
        </form>
        </div>
    </div>

</div>
 <!-- <p>Content here. <a class="alert" href=#>Alert!</a></p> -->

 <!-- Modal -->
<div class="modal fade" id="unverified" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Unverified</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                             <form id="mail_un" method="post">
                              <input type="hidden" name="opera" id="opera" value="unverified_mail"/>
                                <div class="form-group">
                                    <label> Email</label>
                                    <input required="" class="form-control email_user" style="width:100%;" type="text" id="email_un" name="email_un"  value="" placeholder=""  readonly="" />
                                   
                                </div>
                                <div class="form-group">
                              <label for="comment">Comment:</label>
                              <textarea class="form-control validate" required="required" rows="5" id="comment" name="comment"></textarea>
                            </div>

                                  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="send_mail_Unverified">Send Mail</button>
      </div>
  </form>
    </div>
  </div>
</div>

 <!-- modal for account credit  -->

  

<!-- 
 <script type="text/javascript">
            $(function () {
                $('#datetimepicker4').datetimepicker();
            });
            $(function () {
                $('#datetimepicker44').datetimepicker();
            });

        </script> -->

<?php   
    include ("footer.php");
?>
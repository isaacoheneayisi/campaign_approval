
<?php
include_once('includes/config.php');
include_once('includes/func.php');
$page_title = "Transactions";
$section = "transactions";
include ("header.php"); 

?>


        <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            
                    <?php include_once('includes/breadcrumbs/breadcrumbs.php'); ?>
          
                    <div class="clearfix"></div>
                    <!-- END DASHBOARD STATS 1-->
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box green">


                                <div class="portlet-title">
                                    <div class="caption">
<div class="x_panel">
                                        <form class="form-inline" action="includes/get_transactions.php" role="form" id="reportform">
                                            <div class="form-group">
                                                <label for="sdate" style="color:black" >Start Date:</label>
                                                <input type="text"  class="form-control date-picker" data-date-format="dd/mm/yyyy" name="sdate" id="sdate">
                                            </div>
                                            <div class="form-group">
                                                <label style="color:black" for="edate">End Date:</label>
                                                <input type="text" class="form-control date-picker" data-date-format="dd/mm/yyyy" name="edate" id="edate">
                                            </div>
                                            <button type="submit" class="btn btn-primary">Generate</button>
                                        </form>
</div>
                                    </div>
                                    <br/>

                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                     <div id = "transaction_table"> </div>

                            </div>
                            </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->



<?php   
    include ("footer.php");
?>
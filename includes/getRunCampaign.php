<?php
/*
 * For more details
 * please check official documentation of DataTables  https://datatables.net/manual/server-side
 * Coded by charaf JRA
 * RefreshMyMind.com
 */

include_once ('config.php');
//$conn = pg_connect(LOCAL_CONN );
$conn = new mysqli(HOST, USERNAME, PASSWORD, DATABASE) or die('Could not connect to db Server'.mysql_error());
//ini_set('session.cookie_domain', 'myaidfund.com/admin');
session_start();

get_all_data();

function get_all_data() {
    global $conn;
    $sql = "SELECT id,ownerid,country,title,date_created,date_end,amount_goal,amount_achieved,publish_status FROM campaign WHERE publish_status='approved' ORDER BY id DESC";

    $result = $conn->query($sql);
    $output = '
<table id="run_campaign_r" class="table table-striped table-bordered">
                      <thead>
                                    <tr>

                                        <th> id</th>
                                        <th> ownerid</th>
                                        <th> country</th>
                                        <th> title </th>
                                        <th> date_created</th>
                                        <th> date_end</th>
                                        <th> amount_goal</th>
                                        <th> amount_achieved</th>
                                        <th>publish_status</th>
                                        <th> View Details</th>
                                   </tr>
                                </thead>
                     <tbody>
    ';
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_object()) {

            $output .= "
                <tr>
                    <td>$row->id</td>
                    <td>$row->ownerid</td>
                    <td>$row->country</td>
                    <td>$row->title</td>
                    <td>$row->date_created</td>
                    <td>$row->date_end</td>
                    <td>$row->amount_goal</td>
                    <td>$row->amount_achieved</td>
                    <td>$row->publish_status</td>
                    <td><button class='btn green view_details'>Campaign Details</button></td>
                   
                </tr>
";
        }
    } else {
        $output .= '
                <tr>
                     <td colspan="5">No Data Found</td>
                </tr>
           ';
    }

    $output .= '</tbody></table>';
    echo $output;

}

die();
/* IF Query comes from DataTables do the following */
if (!empty($_POST)) {
    /*
     * Database Configuration and Connection using mysqli
    define("MyTable", "smslog");
     */
    define("MyTable", "mfund");
    // define("MyTable", "kb_living");
    // define("MyTable", "mfund");
    /* END DB Config and connection */

    /*
     * @param (string) SQL Query
     * @return multidim array containing data array(array('column1'=>value2,'column2'=>value2...))
     *
     */

    /* Useful $_POST Variables coming from the plugin */
    $draw = $_POST["draw"];//counter used by DataTables to ensure that the Ajax returns from server-side processing requests are drawn in sequence by DataTables
    /* $orderByColumnIndex  = $_POST['order'][0]['column'];// index of the sorting column (0 index based - i.e. 0 is the first record)
    $orderBy = $_POST['columns'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
    $orderType = $_POST['order'][0]['dir']; // ASC or DESC
    $start  = $_POST["start"];//Paging first record indicator.
    $length = $_POST['length'];*/
    //Number of records that the table can display in the current draw
    /* END of POST variables */

    function getData() {
        global $conn;

        $sql = "SELECT id,ownerid,country,title,date_created,date_end,amount_goal,amount_achieved,publish_status FROM campaign WHERE publish_status='approved' ORDER BY id DESC";

        $result = $conn->query($sql);
        $data   = array();
        $res    = $result->fetch_object();
        foreach ($res as $row) {
            $data[] = $row;
        }

        return $data;
    }

    function getData2() {
        $orderByColumnIndex = $_POST['order'][0]['column'];// index of the sorting column (0 index based - i.e. 0 is the first record)
        $orderBy            = $_POST['columns'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
        $orderType          = $_POST['order'][0]['dir'];// ASC or DESC
        $start              = $_POST["start"];//Paging first record indicator.
        $length             = $_POST['length'];
        $key                = $_POST['search']['value'];

        global $conn;
        $sql    = "SELECT * FROM campaign WHERE publish_status ilike '%$key%' ORDER BY $orderBy $orderType LIMIT $length OFFSET $start";
        $result = $conn->query($sql);
        $data   = array();
        $res    = $result->fetch_object();
        foreach ($res as $row) {
            $data[] = $row;
        }
        return $data;
    }

    $recordsTotal = count(getData());

    /* SEARCH CASE : Filtered data */
    if (!empty($_POST['search']['value']) || !empty($_POST['order'][0]['column'])) {

        /* WHERE Clause for searching */
        $data            = getData2();
        $recordsFiltered = $recordsTotal;
        /* END SEARCH */
    } else {
        //$sql = sprintf("SELECT * FROM %s ORDER BY %s %s limit %d , %d ", MyTable ,$orderBy,$orderType ,$start , $length);
        $data            = getData();
        $recordsFiltered = $recordsTotal;
    }

    /* Response to client before JSON encoding */
    $response = array(
        "draw"            => intval($draw),
        "recordsTotal"    => $recordsTotal,
        "recordsFiltered" => $recordsFiltered,
        "data"            => $data,
    );

    echo json_encode($response);

} else {
    echo "NO POST Query from DataTable";
}
?>
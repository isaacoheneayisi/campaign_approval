
<div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                        <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                        <li class="sidebar-toggler-wrapper hide">
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <div class="sidebar-toggler"> </div>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                        </li>


        
    <?php if ($_SESSION['access_level'] == "5" || $_SESSION['access_level'] == "1") :?>

                        <li class="nav-item start ">
                            <a href="index.php" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">Home</span>
                                <!-- <span class="selected"></span> -->
                                <!-- <span class="arrow open"></span> -->
                            </a>                            
                        </li>

    <?php if ($_SESSION['access_level'] == "1") :?>
                        <li class="nav-item  ">
                            <a href="users.php" class="nav-link nav-toggle">
                                <i class="icon-diamond"></i>
                                <span class="title">Users Registration(All)</span>
                                <!-- <span class="arrow"></span> -->
                            </a>
                        </li>
                          <li class="nav-item  ">
                            <a href="personal.php" class="nav-link nav-toggle">
                                <i class="icon-diamond"></i>
                                <span class="title"> Registration(Personal)</span>
                                <!-- <span class="arrow"></span> -->
                            </a>
                        </li>
                          <li class="nav-item  ">
                            <a href="organization.php" class="nav-link nav-toggle">
                                <i class="icon-diamond"></i>
                                <span class="title"> Registration(Organization)</span>
                                <!-- <span class="arrow"></span> -->
                            </a>
                        </li>
<?php endif; ?>

<?php if ($_SESSION['access_level'] == "1") :?>
                        <li class="nav-item  ">
                            <a href="Running_campaign.php" class="nav-link nav-toggle">
                                <i class="icon-diamond"></i>
                                <span class="title">Running Campaigns</span>
                                <!-- <span class="arrow"></span> -->
                            </a>
                        </li>
<?php endif; ?>

    <?php if ($_SESSION['access_level'] == "1") :?>
                        <li class="nav-item  ">
                            <a href="Unapproved_campaign.php" class="nav-link nav-toggle">
                                <i class="icon-diamond"></i>
                                <span class="title">Unapproved Campaigns</span>
                                <!-- <span class="arrow"></span> -->
                            </a>
                        </li>
<?php endif; ?>

        <?php if ($_SESSION['access_level'] == "1") :?>
            <li class="nav-item  ">
                <a href="transaction_report.php" class="nav-link nav-toggle">
                    <i class="icon-diamond"></i>
                    <span class="title">Transactions</span>
                    <!-- <span class="arrow"></span> -->
                </a>
            </li>
        <?php endif; ?>

<?php endif; ?>



		
								
								<li class="nav-item  ">
                                    <a href="index.php?islogout=true" class="nav-link ">
                                    <i class="icon-logout"></i>
                                        <span class="title">LOGOUT<span>                                      
                                    </a>
                                </li> 
                            </ul>
                        </li>

                            </ul>
                        </li>
                    </ul>
                    <!-- END SIDEBAR MENU -->
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
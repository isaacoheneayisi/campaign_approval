<?php
/*
 * For more details
 * please check official documentation of DataTables  https://datatables.net/manual/server-side
 * Coded by charaf JRA
 * RefreshMyMind.com
 */

include_once('config.php');
//$conn = pg_connect(LOCAL_CONN );
$conn = new mysqli(HOST,USERNAME,PASSWORD,DATABASE) or die('Could not connect to db Server' . mysql_error());
 ini_set('session.cookie_domain', '.gvmaghana.com');
session_start();
/* IF Query comes from DataTables do the following */
if (!empty($_POST) ) {
    /*
     * Database Configuration and Connection using mysqli
     define("MyTable", "smslog");
     */
    define("MyTable", "kb_living");

    /* END DB Config and connection */

    /*
     * @param (string) SQL Query
     * @return multidim array containing data array(array('column1'=>value2,'column2'=>value2...))
     *
     */

    /* Useful $_POST Variables coming from the plugin */
    $draw = $_POST["draw"];//counter used by DataTables to ensure that the Ajax returns from server-side processing requests are drawn in sequence by DataTables
   /* $orderByColumnIndex  = $_POST['order'][0]['column'];// index of the sorting column (0 index based - i.e. 0 is the first record)
    $orderBy = $_POST['columns'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
    $orderType = $_POST['order'][0]['dir']; // ASC or DESC
    $start  = $_POST["start"];//Paging first record indicator.
    $length = $_POST['length'];*/
    //Number of records that the table can display in the current draw
    /* END of POST variables */


function getData(){
    global $conn ;                       
                                         
    $sql = "SELECT id,FirstName,MiddleName,SurName,Occupation,Gender,PhoneNumber,Reprensentative,Date_Registered FROM clients  ORDER BY id DESC";

   $result = $conn->query($sql);   
    $data = array();
    $res= $result->fetch_all();
       foreach ($res as $row ) {
        $data[] = $row ;      
    }   
    
    return $data;
}

function getData2(){
    $orderByColumnIndex  = $_POST['order'][0]['column'];// index of the sorting column (0 index based - i.e. 0 is the first record)
    $orderBy = $_POST['columns'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
    $orderType = $_POST['order'][0]['dir']; // ASC or DESC
    $start  = $_POST["start"];//Paging first record indicator.
    $length = $_POST['length'];
    $key = $_POST['search']['value'];

    global $conn;
    $sql = "SELECT * FROM transport_transaction_details WHERE item_code ilike '%$key%' 
ORDER BY $orderBy $orderType
LIMIT $length OFFSET $start";

$result = pg_fetch_object(pg_query($conn,$sql));
    $data = array();
    foreach ($result as $row ) {
        $data[] = $row ;
    }
    return $data;
}

    

    $recordsTotal = count(getData());

    /* SEARCH CASE : Filtered data */
    if(!empty($_POST['search']['value']) || !empty($_POST['order'][0]['column'])){

        /* WHERE Clause for searching */
        $data = getData2();
        $recordsFiltered = $recordsTotal;
    /* END SEARCH */
}
    else {
        //$sql = sprintf("SELECT * FROM %s ORDER BY %s %s limit %d , %d ", MyTable ,$orderBy,$orderType ,$start , $length);
        $data = getData();
        $recordsFiltered = $recordsTotal;
    }

    /* Response to client before JSON encoding */
    $response = array(
        "draw" => intval($draw),
        "recordsTotal" => $recordsTotal,
        "recordsFiltered" => $recordsFiltered,
        "data" => $data
    );

    echo json_encode($response);

} else {
    echo "NO POST Query from DataTable";
}
?>
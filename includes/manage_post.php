<?php

/**
 * Created by PhpStorm.
 * User: Gideon Paitoo
 * Date: 4/12/2016
 * Time: 10:28 AM
 */
session_start();
//$fname = $_SESSION['firstname'];
include_once('config.php');
include_once('func.php');
  
  $conn = new mysqli(HOST,USERNAME,PASSWORD,DATABASE) or die('Could not connect to db Server' . mysql_error());
  global $conn ;

if(isset($_REQUEST['opera']))
{

    $opera = $_REQUEST['opera'];
    if($opera == 'add_clients'){
        add_clients();
    }else if($opera == 'credit_account'){
        credit_account();
    }else if($opera == 'client_balance'){
        get_client_Balance();
    }else if($opera == 'updatepartner'){
        update_partner();
    }else if($opera == 'deletepartner'){
        delete_partner();
    }else if($opera == 'addcredit'){
        add_credit();
    } elseif ($opera == 'add_candidates') {
        add_candidates();
    }elseif ($opera == 'add_students') {  
        add_students();
    }elseif ($opera == 'unverified_mail') {  
        send_unapproval();
    }elseif ($opera == 'change_pub') {  
        change_pub_status();
    }elseif ($opera == 'verified_acc') {  
        send_approval();
    }   else if ($opera == 'doc'){
              get_Path();
     }
}

function get_Path(){
   $id = $_REQUEST['id'];
  // echo $id;
   $doc = get_doc_Path($id);
      $arr = array(
      'doc_path_business'=> $doc      
      );   
    
    echo json_encode($arr);
}


function send_approval(){
    $to = $_REQUEST['email'];
    $camp_id=$_REQUEST['camp_id'];
    update_document($camp_id);
    send_mail_verified($to);
      echo '1';
    
}

function  update_document($id){
 global $conn;

  $sql = "UPDATE account SET document_uploaded  ='1' WHERE id = '$id'";

  $result = $conn->query($sql);
}

function send_unapproval() {
  $to = $_REQUEST['email_un'];
   $comment= $_REQUEST['comment'];
    send_mail_unverified($to,$comment);
      echo '1';
}

function send_mail_unverified($to,$comment) {

 // Always set content-type when sending HTML email
 $headers = "MIME-Version: 1.0"."\r\n";
 $headers .= "Content-type:text/html;charset=UTF-8"."\r\n";
 
 // More headers
 $headers .= 'From: info@myaidfund.com '."\r\n";
 //$headers .= 'Cc: myboss@example.com' . "\r\n";
  $message = get_conf_unmessage($comment);
  $subject= "Unapproved Account ";

 mail($to, $subject, $message, $headers);

}

function send_mail_verified($to) {

 // Always set content-type when sending HTML email
 $headers = "MIME-Version: 1.0"."\r\n";
 $headers .= "Content-type:text/html;charset=UTF-8"."\r\n";
 
 // More headers
  $headers .= 'From: info@myaidfund.com '."\r\n";
 //$headers .= 'Cc: myboss@example.com' . "\r\n";
  $message = get_conf_message();
  $subject= "Approved Account";

 mail($to, $subject, $message, $headers);

}


function  change_pub_status(){
 global $conn;
  $id = $_REQUEST['id'];
  $pub_state = $_REQUEST['status'];
  // echo $id;
  // echo $pub_state;
  $public = "";

  if ($pub_state =="unapproved"){
    $public = "approved";
  } else {
    $public ="unapproved";
  }
 
  $sql = "UPDATE campaign SET publish_status ='$public' WHERE id = '$id'";

  $result = $conn->query($sql);

  if($result){
    
     echo json_encode(true);
  }
    else{
      echo json_encode(false);
    }

}


function get_email ($owerid){
   global $conn;

 $sql = "SELECT email from account  WHERE id = '$ownerid'";
 $result = $conn->query($sql);
 $res="";
 while ($row = $result->fetch_object()) {
     $res = $row->email;
 }

  return $res;
}



function change_status(){
    $id = pg_escape_string($_REQUEST['id']);
    global $conn;

        
    $sql = "UPDATE transport_transaction_details SET item_status='Received' where id='$id'";
    
   

    $result = pg_query($conn,$sql);
    
    if($result){
        $senderF="";
        $senderL="";
        $senderE="";
        $receiverF="";
        $receiverL="";
        $receiverE="";

        $sql2 = "SELECT sender_firstname,sender_lastname,receiver_firstname,receiver_lastname,sender_email,receiver_email  FROM  transport_transaction_details  where id='$id'";
        $result2 = pg_query($conn,$sql2);
        while($r1 = pg_fetch_array($result2))
        {
           $senderF= $r1['sender_firstname'];
           $senderL= $r1['sender_lastname'];
           $senderE=$r1['sender_email'];
           $receiverF=$r1['receiver_firstname'];
           $receiverL=$r1['receiver_lastname'];
           $receiverE=$r1['receiver_email'];
        }
        $submitted="Hello " .$senderF." ".$senderL."<br /> Package Item of description that was sent by you has been successfully received by ".$receiverF." ".$receiverL.".Thank your for using our courier service.<br /> <br /> Best Regards";   
   $sender_msg="Hello " .$receiverF." ".$receiverL."<br /> Thank you for the collection of Items that was sent by  ".$senderF." ".$senderL.".Continue using our courier service for an effective Item delivery.<br /> <br /> Best Regards";     

       sendMail ($senderE,$senderF,$submitted);
       sendMail ($receiverE,$receiverF,$sender_msg);

        echo json_encode(true);
        
    }else{
        echo json_encode(false);
    }

    
}

function getReceiverDetails($id){
  
  
   global $conn;
       
   $sql = "SELECT sender_firstname,sender_lastname,receiver_firstname,receiver_lastname,sender_email,receiver_email  FROM  transport_transaction_details  where id='$id'";
    
}



function  add_clients(){
     global $conn;

            $id = $_POST['client_idd'];
            $fname = $_POST['ffname'];
            $llname = $_POST['llname'];
            $iamount = $_POST['iamount'];
            //$date_deposited = $_POST['datetime'];
            $date1 = $_POST['datetime'];
            $date = date('Y-m-d', strtotime($date1));

      //  echo $id . "" . $fname. "".$iamount. "" .$date_deposited."".$llname;
            $new_balance =  getLastBalanceCredit ($id);
            $new_balance= $new_balance + $iamount;
                                                 
            $sql = "INSERT INTO  transaction(firstname,lastname,Amount,Transaction_type,id_client,Date_transaction) VALUES ('$fname','$llname',$iamount,'Credit',$id,'$date')";
            $sql1 ="INSERT INTO  total_balance(id_client,balance,date_balance) VALUES ($id,$new_balance,'$date')";
   
     //echo $sql;
            $result = $conn->query($sql);   
            $result1 = $conn->query($sql1);   
     
             if($result  && $result1){
            echo true;
            }else{
             echo false;
        }      
    }   

function getLastBalanceCredit ($id){
 global $conn ;                       
                                         
  $sql = "SELECT balance from total_balance where id_client='$id' ORDER BY id DESC limit 1";
    
   $result = $conn->query($sql);   
    $data =0;
    $res= $result->fetch_all();
       foreach ($res as $row ) {
        $data= $row[0] ;      
    }   
    
    return $data;
}


function   credit_account(){
   global $conn;

            $id = $_POST['client_idd'];
            $fname = $_POST['ffname'];
            $llname = $_POST['llname'];
            $iamount = $_POST['iamount'];
            //$date_deposited = $_POST['datetime'];
            $date1 = $_POST['datetime'];
            $date = date('Y-m-d', strtotime($date1));

      //echo $id . "" . $fname. "".$iamount. "" .$date_deposited."".$llname;
         $new_balance =  getLastBalanceCredit ($id);
         if($iamount <=  $new_balance  ){
              $new_balance= $new_balance - $iamount;
                                                 
       $sql = "INSERT INTO  transaction(firstname,lastname,Amount,Transaction_type,id_client,Date_transaction) VALUES ('$fname','$llname',$iamount,'Debit',$id,'$date')";
       $sql1 ="INSERT INTO  total_balance(id_client,balance,date_balance) VALUES ($id,$new_balance,'$date')";
   
     //echo $sql;
      $result = $conn->query($sql);   
      $result1 = $conn->query($sql1);   
     
      if($result  && $result1){
            echo true;
       }else{
             echo false;
        }      
         }{
             echo false;
         }
       
    } 
function get_client_Balance(){

   global $conn ;                       
                   
      $id= $_REQUEST['id'];                                   
  $sql = "SELECT balance from total_balance where id_client='$id' ORDER BY id DESC limit 1";
    
   $result = $conn->query($sql);   
    $data =0;
    $res= $result->fetch_all();
       foreach ($res as $row ) {
        $data= $row[0] ;      
    }   
    
   echo $data;

}
//         $service_name = pg_escape_string($_REQUEST['servicename']);
//         $shortcode = pg_escape_string($_REQUEST['shortcode']);
//         $keywords = pg_escape_string($_REQUEST['keywords']);
//         $service_category = pg_escape_string($_REQUEST['service_category']);
//         $service_description = pg_escape_string($_REQUEST['service_description']);
//         $service_type = pg_escape_string($_REQUEST['service_type']);
//         $delivery_type = pg_escape_string($_REQUEST['delivery_type']);
// //        $sdp_product_id = pg_escape_string($_REQUEST['sdp_product_id']);
//         $networks = pg_escape_string($_REQUEST['networks']);
//         $subscription_message = pg_escape_string($_REQUEST['subscription_message']);
//         $welcome_message = pg_escape_string($_REQUEST['welcome_message']);
//         $renewal_message = pg_escape_string($_REQUEST['renewal_message']);
//         $unsubscription_message = pg_escape_string($_REQUEST['unsubscription_message']);
//         $service_owner = pg_escape_string($_REQUEST['service_owner']);
// //        $billing_cycle = pg_escape_string($_REQUEST['billing_cycle']);
//         $amount = pg_escape_string($_REQUEST['amount']);
//         $delivery_per_day = pg_escape_string($_REQUEST['delivery_per_day']);
// // //        $sdp_id = pg_escape_string($_REQUEST['sdp_id']);

//     global $conn;
//     $recorddoesntexist = checkdupesindb ( "subscription_service", "service_name",$service_name, "Record already exists. Choose another name." );

//     if($recorddoesntexist){

//         $sql = "INSERT INTO subscription_service(service_name,service_shortcode,service_keyword,service_category,service_description,sub_type,service_content_delivery_method,service_network,subscription_msg,welcome_msg,renewal_msg,unsub_msg,service_owner,billing_amt,delivery_per_day) VALUES('$service_name','$shortcode','$keywords',
// '$service_category','$service_description','$service_type','$delivery_type','$networks','$subscription_message',
// '$welcome_message','$renewal_message','$unsubscription_message','$service_owner','$amount','$delivery_per_day');";
//         $result = pg_query( $conn, $sql );
//         if($result){
//             echo true;
//         }else{
//             echo false;
//         }
//     }



function change_service(){
        $service_id = $_REQUEST['id'];
        $service_name = pg_escape_string($_REQUEST['servicename']);
        $shortcode = pg_escape_string($_REQUEST['shortcode']);
        $keywords = pg_escape_string($_REQUEST['keywords']);
        $service_category = pg_escape_string($_REQUEST['service_category']);
        $service_description = pg_escape_string($_REQUEST['service_description']);
        $service_type = pg_escape_string($_REQUEST['service_type']);
        $delivery_type = pg_escape_string($_REQUEST['delivery_type']);
//        $sdp_product_id = pg_escape_string($_REQUEST['sdp_product_id']);
        $networks = pg_escape_string($_REQUEST['networks']);
        $subscription_message = pg_escape_string($_REQUEST['subscription_message']);
        $welcome_message = pg_escape_string($_REQUEST['welcome_message']);
        $renewal_message = pg_escape_string($_REQUEST['renewal_message']);
        $unsubscription_message = pg_escape_string($_REQUEST['unsubscription_message']);
        $service_owner = pg_escape_string($_REQUEST['service_owner']);
//        $billing_cycle = pg_escape_string($_REQUEST['billing_cycle']);
        $amount = pg_escape_string($_REQUEST['amount']);
        $delivery_per_day = pg_escape_string($_REQUEST['delivery_per_day']);
//        $sdp_id = pg_escape_string($_REQUEST['sdp_id']);

    global $conn;
    $recorddoesntexist = checkdupesindb ( "subscription_service", "service_name",$service_name, "Record already exists. Choose another name." );

    if(!$recorddoesntexist){

        $sql = "UPDATE subscription_service SET service_name='$service_name',service_shortcode='$shortcode',service_keyword='$keywords',service_category='$service_category',service_description='$service_description',sub_type='$service_type',service_content_delivery_method='$delivery_type',service_network='$networks',subscription_msg='$subscription_message',welcome_msg='$welcome_message',renewal_msg='$renewal_message',unsub_msg='$unsubscription_message',service_owner='$service_owner',billing_amt='$amount',delivery_per_day='$delivery_per_day' WHERE id = $service_id";

/*        $sql = "INSERT INTO subscription_service(service_name,service_shortcode,service_keyword,service_category,service_description,sub_type,service_content_delivery_method,service_network,subscription_msg,welcome_msg,renewal_msg,unsub_msg,service_owner,billing_amt,delivery_per_day) VALUES('$service_name','$shortcode','$keywords','$service_category','$service_description','$service_type','$delivery_type','$networks','$subscription_message',
'$welcome_message','$renewal_message','$unsubscription_message','$service_owner','$amount','$delivery_per_day');";*/

        $result = pg_query( $conn, $sql );
        if($result){
            echo true;
        }else{
            echo false;
        }
    }

}



function add_content(){
    global $fname;
    global $conn;
    $msg_title = pg_escape_string($_REQUEST['sms_title']);
    $sms_service = pg_escape_string($_REQUEST['sms_service']);
    $publication_date = pg_escape_string($_REQUEST['publication_date']);
    $publication_date = date('Y-m-d',strtotime($publication_date));
    //echo $publication_date;
    $sms_content = pg_escape_string($_REQUEST['sms_content']);

    $sql = "INSERT INTO mtechghana2.content_messages(title,cat_id,pubdate,description,userid,lastupdate,is_approved) VALUES('$msg_title','$sms_service','$publication_date','$sms_content','$fname','NOW()',1)";
    $result = pg_query ( $conn, $sql );
    if($result){
        echo true;
    }else{
        echo false;
    }
    //echo $result;
}

function change_content(){
    global $fname;
    global $conn;
    $content_id = $_REQUEST['id'];
    $msg_title = pg_escape_string($_REQUEST['sms_title']);
    $sms_service = pg_escape_string($_REQUEST['sms_service']);
    $publication_date = pg_escape_string($_REQUEST['publication_date']);
    $publication_date = date('Y-m-d',strtotime($publication_date));
    //echo $publication_date;
    $sms_content = pg_escape_string($_REQUEST['sms_content']);
    $sql = "UPDATE mtechghana2.content_messages SET title = '$msg_title', cat_id = '$sms_service', pubdate='$publication_date',description='$sms_content',lastupdate='NOW()' WHERE id = $content_id";
    $result = pg_query ( $conn, $sql );
    if($result){
        echo true;
    }else{
        echo false;
    }
    //echo $result;
}

function add_partner(){
    $company = pg_escape_string($_REQUEST['company_name']);
    //$cost = pg_escape_string($_REQUEST['cost']);
   // $account_type = pg_escape_string($_REQUEST['account_type']);
   // $email = pg_escape_string($_REQUEST['email']);
    //$fname = pg_escape_string($_REQUEST['fname']);
    $fname = pg_escape_string($_REQUEST['fname']);
    $phone1 = pg_escape_string($_REQUEST['phone1']);
    //$phone2 = pg_escape_string($_REQUEST['phone2']);
    $xxname = pg_escape_string($_REQUEST['xxname']);    
    $xxpass = md5($_REQUEST['xxpass']);
    $active = 0;
    if(isset($_REQUEST['active'])){
        $active = 1;
    }else{
        $active = 0;
    }
    //$api = md5(getToken(7));
    //$createdby = $_SESSION['firstname'];



    global $conn;
    //$sql = "INSERT INTO account(org,firstname,lastname,email,phone1,phone2,apikey,accounttype,costpersms,active) VALUES('$company','$fname','$lname','$email','$phone1','$phone2','$api','$account_type',$cost,$active); INSERT INTO letin(smsowner,firstname,lastname,xxname,xxpass,access_level,active) VALUES('$api','$fname','$lname','$xxname','$xxpass','1',$active);";
   
      
    $sql = "INSERT INTO schools(school_name,event_title,phone_numbers) VALUES('$company','$fname','$phone1'); INSERT INTO users(firstname,lastname,login,pw,access_level,active) VALUES('$company','$fname','$xxname','$xxpass','1',$active);";
 
    $result = pg_query($conn,$sql);
    if($result){
        echo json_encode(true);
        
    }else{
        echo json_encode(false);
    }
}



function add_transport_details(){
   
    $kilo     = pg_escape_string($_REQUEST['kilo_weight']);
    $pricetagg = pg_escape_string($_REQUEST['pricetagg']);
    $fsender = pg_escape_string($_REQUEST['fsender']);
    $lsender = pg_escape_string($_REQUEST['lsender']);
    $phone1  = pg_escape_string($_REQUEST['phone1']);
    $email   = pg_escape_string($_REQUEST['email']);
    $desc_item = pg_escape_string($_REQUEST['desc_item']);
    $rfirst = pg_escape_string($_REQUEST['rfirst']);
    $rlast =  pg_escape_string($_REQUEST['rlast']);
    $rphone = pg_escape_string($_REQUEST['rphone']);
    $remail = pg_escape_string($_REQUEST['remail']);
    $source = pg_escape_string($_REQUEST['source']);
    $destination = pg_escape_string($_REQUEST['destination']);

      $pass = ''; 

    $chars = "abcdefghijkmnopqrstuvwxyz023456789"; 

    srand((double)microtime()*1000000); 
    $i = 0; 
    while ($i <= 7) { 
        $num = rand() % 33; 
        $tmp = substr($chars, $num, 1); 
        $pass = $pass . $tmp; 
        $i++; 
    } 
     $getCode = $pass;

    global $conn;
    

        
    $sql = "INSERT INTO transport_transaction_details(item_kilo,price,sender_firstname,sender_lastname,sender_number,sender_email,item_description,receiver_firstname,receiver_lastname,receiver_number,receiver_email,item_code,source,destination,item_status,date_added) VALUES('$kilo','$pricetagg','$fsender','$lsender','$phone1','$email','$desc_item','$rfirst','$rlast','$rphone','$remail','$getCode','$source','$destination','submitted',NOW());";
   
    $result = pg_query($conn,$sql);
    if($result){
         $submitted="Hello " .$rfirst." ".$rlast."<br /> Item of package "." ".$desc_item." ". "from"." ".$fsender." ".$lsender." ". 
"has been sent by courrier to ".$rfirst." ".$rlast.".Please visit our office at your location with code"." ". "<b>".$getCode."</b>. for identification and collection.<br /> <br /> Best Regards";   
 $sender_msg="Hello " .$lsender." ".$fsender."<br /> Item of package "." ".$desc_item." ". 
"has been sent by courrier to ".$rfirst." ".$rlast.".Please ensure he visits our office at his location with code"." ". "<b>".$getCode."</b>. for identification and collection.<br /> <br /> Best Regards";     

       sendMail ($remail,$rfirst,$submitted);
       sendMail ($email,$lsender,$sender_msg);
        echo json_encode(true);
        
    }else{
        echo json_encode(false);
    }

}
function add_candidates(){

    $company = pg_escape_string($_REQUEST['candidate_name']);  
    $school_name = pg_escape_string($_REQUEST['school']);
    
    global $conn;
    
    //$sql = "SELECT id FROM schools where school_name ilike '%$school_name%'";
    //echo $sql;
   // $result = pg_query ($conn, $sql);


    $sql2 = "INSERT INTO candidates(candidate_name,school_id) VALUES('$company',$school_name);";
 
    $result2 = pg_query($conn,$sql2);
    

    if($result2){
        echo json_encode(true);
        
    }else{
        echo json_encode(false);
    }
}
  

  function genCode(){
     
     $pass = ''; 

    $chars = "abcdefghijkmnopqrstuvwxyz023456789"; 

    srand((double)microtime()*1000000); 
    $i = 0; 
    while ($i <= 7) { 
        $num = rand() % 33; 
        $tmp = substr($chars, $num, 1); 
        $pass = $pass . $tmp; 
        $i++; 
    } 
    return $pass; 
  }

  function add_students(){

    //$company = pg_escape_string($_REQUEST['candidate_name']);  
    $school_name = pg_escape_string($_REQUEST['school']);
    
    global $conn;
    

     $genCode= genCode();

    //$sql = "SELECT id FROM schools where school_name ilike '%$school_name%'";
    //echo $sql;
   // $result = pg_query ($conn, $sql);


    $sql2 = "INSERT INTO students(students_id,school_id) VALUES('$genCode','$school_name');";
 
    $result2 = pg_query($conn,$sql2);
    

    if($result2){
        echo json_encode(1);
        
    }else{
        echo json_encode(0);
    }
}
  

function update_partner(){
    $company = pg_escape_string($_REQUEST['company_name']);
    $cost = pg_escape_string($_REQUEST['cost']);
    $account_type = pg_escape_string($_REQUEST['account_type']);
    $email = pg_escape_string($_REQUEST['email']);
    $fname = pg_escape_string($_REQUEST['fname']);
    $lname = pg_escape_string($_REQUEST['lname']);
    $phone1 = pg_escape_string($_REQUEST['phone1']);
    $phone2 = pg_escape_string($_REQUEST['phone2']);
/*    $xxname = pg_escape_string($_REQUEST['xxname']);    
    $xxpass = md5($_REQUEST['xxpass']);*/
    $apikey = $_REQUEST['apikey'];
    $active = 0;
    if(isset($_REQUEST['active'])){
        $active = 1;
    }else{
        $active = 0;
    }
    // $api = md5(getToken(7));
    //$createdby = $_SESSION['firstname'];



    global $conn;
    $sql = "UPDATE account SET org = '$company', firstname = '$fname',lastname='$lname',email = '$email',phone1='$phone1',phone2='$phone2',accounttype='$account_type',costpersms=$cost,active=$active WHERE apikey = '$apikey'; UPDATE letin SET firstname='$fname',lastname='$lname',active=$active WHERE smsowner = '$apikey';";

    $result = pg_query($conn,$sql);
    if($result){
        echo json_encode(true);
        
    }else{
        echo json_encode(false);
    }
}


function add_credit(){    
    $amount = pg_escape_string($_REQUEST['amount']);
    $apikey = $_REQUEST['apikey'];
    $user = $_SESSION['username'];

    global $conn;
    $sql = "UPDATE account SET credit = credit + $amount WHERE apikey = '$apikey'; INSERT INTO trans(transowner,transwhen,transamount,transagent) VALUES('$apikey',NOW(),$amount,'$user');";

    $result = pg_query($conn,$sql);
    if($result){
        echo json_encode(true);
        
    }else{
        echo json_encode(false);
    }
}


function delete_partner(){
    $api = $_REQUEST['apikey'];


    global $conn;
    $sql = "DELETE FROM account WHERE apikey = '$api'; DELETE FROM letin WHERE smsowner = '$api';";

    $result = pg_query($conn,$sql);
    if($result){
        echo json_encode(true);
        
    }else{
        echo json_encode(false);
    }
}

function checkdupesindb($table, $columntocheck, $valuetocheck, $messagetoadd) {
    global $message;
    global $conn;
    $sql = "SELECT count(*) as cnt from $table where $columntocheck = '$valuetocheck';";
    //echo $sql;
    $result = pg_query ( $conn, $sql );
    //$row = pg_fetch_all( $result );

    while($r1 = pg_fetch_array($result))
    {
        if ($r1 ['cnt'] == 0) {
            return true;
        } else {
            $message [] = $messagetoadd;
            return false;
        }
    }

}

function crypto_rand_secure($min, $max)
{
    $range = $max - $min;
    if ($range < 1) return $min; // not so random...
    $log = ceil(log($range, 2));
    $bytes = (int) ($log / 8) + 1; // length in bytes
    $bits = (int) $log + 1; // length in bits
    $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
    do {
        $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
        $rnd = $rnd & $filter; // discard irrelevant bits
    } while ($rnd >= $range);
    return $min + $rnd;
}

function getToken($length)
{
    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
    $codeAlphabet.= "0123456789";
    $max = strlen($codeAlphabet) - 1;
    for ($i=0; $i < $length; $i++) {
        $token .= $codeAlphabet[crypto_rand_secure(0, $max)];
    }
    return $token;
}

function get_conf_message()
{

   $msg = '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<div bgcolor="#F6F5F2" style="background:#f6f5f2;margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:0;padding-right:0;padding-bottom:0;padding-left:0">



    <table align="center" width="100%" cellspacing="0" cellpadding="0" bgcolor="#F6F5F2" style="background:#f6f5f2;margin-top:0;text-align:center;width:100%">
        <tbody>

 <tr>
            <td style="min-width:8px"></td>
            <td height="18px">&nbsp;</td>
            <td style="min-width:8px"></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tbody>
                    <tr>
                        <td align="left" style="text-align:left">
                            <a href="">
                                <img src="https://myaidfund.com/assets/img/logo.png" width="111" height="" border="0" style="border:none">
                            </a>
                        </td>
                       
                    </tr>
                    </tbody>
                </table>
            </td>
            <td>&nbsp;</td>
        </tr>


        <tr>
            <td style="min-width:8px"></td>
            <td height="18px">&nbsp;</td>
            <td style="min-width:8px"></td>
        </tr>
       
        <tr>
            <td>&nbsp;</td>
            <td height="23px">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td align="center" width="500" style="text-align:center">


                <table align="center" cellspacing="0" cellpadding="0" border="0" width="100%" bgcolor="#FFFFFF" style="background:#ffffff;border-width:1px;border-style:solid;border-color:#e6e6e6;border-radius:5px 5px 5px 5px;max-width:500px;min-width:320px;text-align:left;width:100%">
                    <tbody>
                    <tr>
                        <td>

                            <table align="center" style="width:100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>

                                <tr>
                                    <td width="14">&nbsp;</td>
                                    <td height="31">&nbsp;</td>
                                    <td width="14">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td width="14">&nbsp;</td>
                                    <td>

                                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="width:100%">
                                            <tbody>

                                            <tr>
                                                <td height="8" style="font-size:8px;line-height:8px">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td height="1" style="border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d4d4d4;font-size:16px;line-height:16px">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td height="25">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="center">

                                                    <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                        <tbody>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td width="320">

                                                                <table align="center" cellspacing="0" cellpadding="0" border="0" style="width:100%;max-width:320px">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td style="color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;line-height:24px">
                                                                             We are happy to inform you that your identification document(s) have been verified successfully.         Please proceed to login to your account and create your campaign to start accepting donations.<br />
                                                                              Thank you
                                                                              <br />
                                                                             <p style="color:red"> NB: You can also contact our support team on the website, for any clarification </p> <br />
                                                                             <p> <a href="https://myaidfund.com/" class="btn btn-primary btn-lg disabled" role="button" aria-disabled="true">Login to your account</a> </p>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr><tr>
                                                <td height="13" style="font-size:13px;line-height:13px">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td height="1" style="border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d4d4d4;font-size:16px;line-height:16px">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td height="30">&nbsp;</td>
                                            </tr>
                                          
                                            <tr>
                                                <td height="25">&nbsp;</td>
                                            </tr>
                                          

                                            </tbody>
                                        </table>

                                    </td>
                                    <td width="14">&nbsp;</td>
                                </tr>

                                </tbody>
                            </table>


                        </td>
                    </tr>


                    <tr>
                        <td>

                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tbody>
                                <tr>
                                    <td width="14">&nbsp;</td>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td height="25">&nbsp;</td>
                                            </tr>
                                          
                                            <tr>
                                                <td height="15">&nbsp;</td>
                                            </tr>
                                           
                                          
                                            <tr>
                                                <td height="30">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="14">&nbsp;</td>
                                </tr>
                                </tbody>
                            </table>

                        </td>
                    </tr>

                    </tbody>
                </table>
            </td>
            <td></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td height="26"></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="color:#999999;font-family:Helvetica,Arial,sans-serif;line-height:20px;font-size:12px">
                Sent from MyAidfund\'s Headquarters:
                <br>
                <a href="#" style="color:#999999!important;text-decoration:none!important;color:#999999;text-decoration:none">P. O. Box AH 42 Achimota</a>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td height="50"></td>
            <td>&nbsp;</td>
        </tr>
        </tbody>
    </table>

    <div style="display:none;white-space:nowrap;font:15px courier;color:#f6f5f2">
        - - - - - - - - - - - - - - - - - - -
    </div>


    <img src="" alt="" width="2" height="2" class="CToWUd" style=""></div>
</body>
</html>';
    return $msg;
}

function get_conf_unmessage($comment)
{
     $msg = '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<div bgcolor="#F6F5F2" style="background:#f6f5f2;margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:0;padding-right:0;padding-bottom:0;padding-left:0">



    <table align="center" width="100%" cellspacing="0" cellpadding="0" bgcolor="#F6F5F2" style="background:#f6f5f2;margin-top:0;text-align:center;width:100%">
        <tbody>
         <tr>
            <td style="min-width:8px"></td>
            <td height="18px">&nbsp;</td>
            <td style="min-width:8px"></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tbody>
                    <tr>
                        <td align="left" style="text-align:left">
                            <a href="">
                                <img src="https://myaidfund.com/assets/img/logo.png" width="111" height="" border="0" style="border:none">
                            </a>
                        </td>
                        
                    </tr>
                    </tbody>
                </table>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="min-width:8px"></td>
            <td height="18px">&nbsp;</td>
            <td style="min-width:8px"></td>
        </tr>
       
        <tr>
            <td>&nbsp;</td>
            <td height="23px">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td align="center" width="500" style="text-align:center">


                <table align="center" cellspacing="0" cellpadding="0" border="0" width="100%" bgcolor="#FFFFFF" style="background:#ffffff;border-width:1px;border-style:solid;border-color:#e6e6e6;border-radius:5px 5px 5px 5px;max-width:500px;min-width:320px;text-align:left;width:100%">
                    <tbody>
                    <tr>
                        <td>

                            <table align="center" style="width:100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>

                                <tr>
                                    <td width="14">&nbsp;</td>
                                    <td height="31">&nbsp;</td>
                                    <td width="14">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td width="14">&nbsp;</td>
                                    <td>

                                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="width:100%">
                                            <tbody>

                                            <tr>
                                                <td height="8" style="font-size:8px;line-height:8px">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td height="1" style="border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d4d4d4;font-size:16px;line-height:16px">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td height="25">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="center">

                                                    <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                        <tbody>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td width="320">

                                                                <table align="center" cellspacing="0" cellpadding="0" border="0" style="width:100%;max-width:320px">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td style="color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;line-height:24px">
                                                                               We are sorry to bring to your notice that your identification document(s) were not able to be verified due to the following reasons below: .
                                                                              <br />
                                                                             <p style="color:red"> 
                                                                              '.$comment.'
                                                                              </p>
                                                                              <br />
                                                                             <p> <a href="https://myaidfund.com/" class="btn btn-primary btn-lg disabled" role="button" aria-disabled="true">Login to make changes</a> </p>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr><tr>
                                                <td height="13" style="font-size:13px;line-height:13px">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td height="1" style="border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#d4d4d4;font-size:16px;line-height:16px">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td height="30">&nbsp;</td>
                                            </tr>
                                          
                                            <tr>
                                                <td height="25">&nbsp;</td>
                                            </tr>
                                          

                                            </tbody>
                                        </table>

                                    </td>
                                    <td width="14">&nbsp;</td>
                                </tr>

                                </tbody>
                            </table>


                        </td>
                    </tr>


                    <tr>
                        <td>

                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tbody>
                                <tr>
                                    <td width="14">&nbsp;</td>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td height="25">&nbsp;</td>
                                            </tr>
                                          
                                            <tr>
                                                <td height="15">&nbsp;</td>
                                            </tr>
                                           
                                            <tr>
                                                <td style="color:#999999;font-family:Helvetica,Arial,sans-serif;line-height:25px;font-size:14px;text-align:center">
                                                    Don\'t want these emails?
                                                    <br>
                                                    <a href="https://www.myaidfund.com/unsubscribe.php" style="display:inline-block;color:#5f7d19!important;color:#5f7d19;text-decoration:underline!important;text-decoration:underline" target="_blank" data-saferedirecturl="">Unsubscribe</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="30">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="14">&nbsp;</td>
                                </tr>
                                </tbody>
                            </table>

                        </td>
                    </tr>

                    </tbody>
                </table>
            </td>
            <td></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td height="26"></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="color:#999999;font-family:Helvetica,Arial,sans-serif;line-height:20px;font-size:12px">
                Sent from MyAidfund\'s Headquarters:
                <br>
                <a href="#" style="color:#999999!important;text-decoration:none!important;color:#999999;text-decoration:none">P. O. Box AH 42 Achimota</a>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td height="50"></td>
            <td>&nbsp;</td>
        </tr>
        </tbody>
    </table>

    <div style="display:none;white-space:nowrap;font:15px courier;color:#f6f5f2">
        - - - - - - - - - - - - - - - - - - -
    </div>


    <img src="" alt="" width="2" height="2" class="CToWUd" style=""></div>
</body>
</html>';
    return $msg;
}



<?php
/*
 * For more details
 * please check official documentation of DataTables  https://datatables.net/manual/server-side
 * Coded by charaf JRA
 * RefreshMyMind.com
 */

include_once ('config.php');
//$conn = pg_connect(LOCAL_CONN );
$conn = new mysqli(HOST, USERNAME, PASSWORD, DATABASE) or die('Could not connect to db Server'.mysql_error());
//ini_set('session.cookie_domain', 'myaidfund.com/admin');
session_start();
$sdate = $_REQUEST['sdate'];
$edate = $_REQUEST['edate'];
$sd = (date_parse_from_format("d/m/Y",$sdate));
$ed = (date_parse_from_format("d/m/Y",$edate));

$startdate = $sd['year'].'-'.$sd['month'].'-'.$sd['day'];
$enddate = $ed['year'].'-'.$ed['month'].'-'.$ed['day'];

get_all_data($startdate,$enddate);

function get_all_data($sdate,$edate) {
    global $conn;
    $sql = "
    SELECT t.transid, t.payment_mode, t.gateway, t.currency_type, t.gross_amount, t.transaction_fee_visa, t.transaction_fee_mm,
     t.gateway_net, t.platform_fee, t.other_revenue, t.total_revenue, t.total_deduction_per_transaction, t.net_payable,t.total_revenue_ghc,t.net_payable_ghc,t.campaign_currency,t.net_payable_campaign_currency, 
     t.logwhen, c.title, a.org
FROM transaction_reports AS t
LEFT JOIN campaign AS c ON c.id = t.campid
LEFT JOIN account AS a ON a.id = t.ownerid
WHERE t.transstate=1 AND '$sdate' <= t.logwhen AND t.logwhen <= '$edate'
    ";


    $result = $conn->query($sql);

    $output = '
<table id="run_campaign" class="table table-striped table-bordered">
                      <thead>
                                    <tr>

                                        <th> Owner</th>
                                        <th> Campaign</th>
                                        <th> Mode of Payment</th>
                                        <th> Gateway </th>
                                        <th> Currency </th>
                                        <th> Gross </th>
                                        <th> Transaction Fee (visa/mastercard)</th>
                                        <th> Transaction Fee (Mobile Money)</th>
                                        <th> Gateway Net </th>
                                        <th> Platform Fee </th>
                                        <th> Other Revenue </th>
                                        <th> Total Revenue </th>
                                        <th> Total Deductions </th>
                                        <th> Donation Net </th>
                                        <th>Campaign Currency</th>
                                        <th>Net Campaign Currency</th>
                                        <th> Total Revenue GHS </th>
                                        <th> Donation Net GHS</th>
                                   </tr>
                                </thead>
                     <tbody>
    ';
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_object()) {

            $output .= "
                <tr>
                    <td>$row->org</td>
                    <td>$row->title</td>
                    <td>$row->payment_mode</td>
                    <td>$row->gateway</td>
                    <td>$row->currency_type</td>
                    <td>$row->gross_amount</td>
                    <td>$row->transaction_fee_visa</td>
                    <td>$row->transaction_fee_mm</td>
                    <td>$row->gateway_net</td>
                    <td>$row->platform_fee</td>
                    <td>$row->other_revenue</td>
                    <td>$row->total_revenue</td>
                    <td>$row->total_deduction_per_transaction</td>
                    <td>$row->net_payable</td>
                    
                    <td>$row->campaign_currency</td>
                    <td>$row->net_payable_campaign_currency</td>
    
                   <td>$row->total_revenue_ghc</td>
                   <td>$row->net_payable_ghc</td>
                </tr>
";
        }
    } else {
        $output .= '
                <tr>
                     <td colspan="14">No Data Found</td>
                </tr>
           ';
    }

    $output .= '</tbody></table>';
    echo $output;

}
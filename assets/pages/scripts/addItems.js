var add_user_form = $("#add_partner").validate({
	rules: {
		kilo_weight: "required",
		fsender: "required",
		lsender: "required",
		
		xxname: {
			required: true,
			minlength: 3,
			remote: {
			        url: "includes/manage_user.php",
			        type: "post",
			        data: {
			        xxname: function() {
			            return $( "#xxname").val();
			        },
			        opera: function(){
			        	return 'checkuser';
			        }
			    }
			}
  
		},
		phone1: {
			required: true,
			minlength: 10
		},
		kilo_weight: {
			required: true
		},
		fsender: {
			required: true
		},
		fsender: {
			required: true
		},
		xxpass: {
			required: true,
			minlength: 5
		},
		confirm_xxpass: {
			required: true,
			minlength: 5,
			equalTo: "#xxpass"
		},
		email: {
			
			email: true
		},
		phone1:"required",
		phone2 :"required",
		fname:"required",
		fsender:"required"			
	},
	messages: {
		kilo_weight: "Please enter the weight of the Item",
		fsender:"Please enter your firstname",
		lsender:"Pleae enter your surname",
		phone1:"Enter a valid phone number and a minimum of 10 digits",
		fname:"Please enter the name of the contact person's first name",
		xxname: {
			required: "Please enter a username",
			minlength: "Your username must consist of at least 3 characters"
		},
		xxpass: {
			required: "Please provide a password",
			minlength: "Your password must be at least 5 characters long"
		},
		confirm_xxpass: {
			required: "Please provide a password",
			minlength: "Your password must be at least 5 characters long",
			equalTo: "Please enter the same password as above"
		},
		email: "Please enter a valid email address"
		
	}
});

$('#add_partner').submit(function(e){
e.preventDefault();
console.log($("#add_partner").valid());
if($("#add_partner").valid()){

	$.blockUI({ css: { 
            border: 'none', 
            padding: '15px',
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        } }); 


    var formData = new FormData($('#add_partner')[0]);
    var ajaxRequest = $.ajax({
        type: "POST",
        url: 'includes/manage_post.php',
        contentType: false,
        processData: false,
        data:formData

    });
    ajaxRequest.done(function (xhr, textStatus) {
    	$.unblockUI();
        console.log(xhr);
        if(xhr=='true') {
          toastr.success("New Partner Has Been Added Successfully", "Status");
           $('#add_partner')[0].reset();
        }else {
            toastr.error("Sorry An Error Occured Please Check and Try Again", "Status")
           
        }

    });

  }
 });

$("#kilo").change(function() {
	var test = $("#kilo").val();
   //alert (test);
   if (test === "1-5"){
   	
         $('#lprice').val("5.00");
     	  $('#llprice').val("5");
     	   $('#pricetag').val("5");
   } else if (test === "5-10") {
   	 $('#lprice').val("10.00");
   	 $('#llprice').val("10");
   	  $('#pricetag').val("10");
   } else if (test === "10-15") {
   	 $('#lprice').val("15.00");
   	 $('#llprice').val("15");
   	  $('#pricetag').val("15");
   } else if (test === "15-20") {
   	 $('#lprice').val("20.00");
   	 $('#llprice').val("20");
   	  $('#pricetag').val("20");
     }else if (test == "above 20"){
   	 $('#lprice').val("30.00");
   	  $('#llprice').val("30");
   	   $('#pricetag').val("30");
   }else {
   	$('#lprice').val("");
   	$('#llprice').val("");
   	 $('#pricetag').val("");
   }



});




                                 
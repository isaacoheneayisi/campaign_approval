
/*$.validator.setDefaults({
		submitHandler: function() {
			alert("submitted!");
		}
	});*/

		// validate the comment form when it is submitted
	

		// validate signup form on keyup and submit
		var add_user_form = $("#add_user_form").validate({
			rules: {
				fname: "required",
				lname: "required",
				xxname: {
					required: true,
					minlength: 5,
					remote: {
					        url: "includes/manage_user.php",
					        type: "post",
					        data: {
					        xxname: function() {
					            return $( "#xxname").val();
					        },
					        opera: function(){
					        	return 'checkuser';
					        }
					    }
					}
          
				},
				xxpass: {
					required: true,
					minlength: 5
				},
				confirm_xxpass: {
					required: true,
					minlength: 5,
					equalTo: "#xxpass"
				},
				email: {
					required: true,
					email: true
				},
				user_group:"required"			
			},
			messages: {
				fname: "Please enter your firstname",
				lname: "Please enter your lastname",
				xxname: {
					required: "Please enter a username",
					minlength: "Your username must consist of at least 5 characters"
				},
				xxpass: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long"
				},
				confirm_xxpass: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long",
					equalTo: "Please enter the same password as above"
				},
				email: "Please enter a valid email address"
				
			}
		});
	
	


$('#user_group').on('change',function(){
	val = $(this).val();
	if(val == '111'){
		$('.network').removeClass('hide');
	}else{
		$('.network').addClass('hide');
	}
});



$('#add_user_form').submit(function(e){
e.preventDefault();
console.log($("#add_user_form").valid());
if($("#add_user_form").valid()){

	$.blockUI({ css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        } }); 


    var formData = new FormData($('#add_user_form')[0]);
    var ajaxRequest = $.ajax({
        type: "POST",
        url: 'includes/manage_user.php',
        contentType: false,
        processData: false,
        data:formData

    });
    ajaxRequest.done(function (xhr, textStatus) {
    	$.unblockUI();
        console.log(xhr);
        if(xhr==1) {
          toastr.success("New User Has Been Added Successfully", "Status");
           $('#add_user_form')[0].reset();
        }else {
            toastr.error("Sorry An Error Occured Please Check and Try Again", "Status")
           
        }

    });

  }
 });


$('#xxnameb').on('change',function(){

var val = $(this).val();
var formdata = new FormData();
formdata.append('opera','checkuser');
formdata.append('xxname',val);

    var ajaxRequest = $.ajax({
        type: "POST",
        url: 'includes/manage_user.php',
        contentType: false,
        processData: false,
        data:formdata

    });
    ajaxRequest.done(function (xhr, textStatus) {
        console.log(xhr);
        if(xhr==1) {
            document.getElementById('xxname').setCustomValidity('');
        }else {
            //toastr.error("Sorry An Error Occured Please Check and Try Again", "Status")
           document.getElementById('xxname').setCustomValidity('Login Name is not Available');
        }

    });
});
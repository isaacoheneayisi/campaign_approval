
$(document).ready(function() {
    //setReport();
    get_transactions();
} );


function get_transactions(){

    $('#reportform').ajaxForm({
        beforeSend: function() {
            $.blockUI({ css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                } });

        },
        uploadProgress: function(event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            console.log(percentVal);
            $('.blockUI h1').html(percentVal);
        },
        success: function() {

        },
        complete: function(xhr) {
            $.unblockUI();
            var rst = xhr['responseText'];

            $('#transaction_table').html(rst);
            $('#run_campaign').DataTable({
                "scrollX": true,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });

        }
    });
}
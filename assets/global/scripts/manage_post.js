/**
 * Created by Gideon Paitoo on 4/12/2016.
 */
$(document).ready(function(){
    toastr.options = {
  "closeButton": true,
  "debug": false,
  "positionClass": "toast-top-center",
  "onclick": null,
  "showDuration": "1000",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

var options = {
            'maxCharacterSize': 160,
            'originalStyle': 'originalTextareaInfo',
            'warningStyle' : 'warningTextareaInfo',
            'warningNumber': 40,
            'displayFormat' : '#input/#max | #words words'
        };
        $('#sms_content').textareaCount(options);

});

$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);


$('#addcontent').submit(function(e){
    e.preventDefault();

    var formData = new FormData($('#addcontent')[0]);
    var ajaxRequest = $.ajax({
        type: "POST",
        url: 'includes/manage_post.php',
        contentType: false,
        processData: false,
        data:formData

    });
    ajaxRequest.done(function (xhr, textStatus) {
        console.log(xhr);
        if(xhr==1) {
            toastr.success("New Content Has Been Inserted Successfully", "Status");
            $('#sms_content').val('');
            //$('#sms_title').val('');
            // $('#addcontent')[0].reset();
           
        }else {
            toastr.error("Sorry An Error Occured Please Check and Try Again", "Status")
            console.log(xhr);
        }

    })
});

$('#changecontent').submit(function(e){
    e.preventDefault();

    var formData = new FormData($('#changecontent')[0]);
    var ajaxRequest = $.ajax({
        type: "POST",
        url: 'includes/manage_post.php',
        contentType: false,
        processData: false,
        data:formData

    });
    ajaxRequest.done(function (xhr, textStatus) {
        console.log(xhr);
        if(xhr==1) {
            toastr.success("Content Has Been Changed Successfully", "Status");
            $('#sms_content').val('');
            //$('#sms_title').val('');
            $('#changecontent')[0].reset();
           
        }else {
            toastr.error("Sorry An Error Occured Please Check and Try Again", "Status")
            console.log(xhr);
        }

    })
});

$('#addservice').submit(function(e){
    e.preventDefault();

    var formData = new FormData($('#addservice')[0]);
    var ajaxRequest = $.ajax({
        type: "POST",
        url: 'includes/manage_post.php',
        contentType: false,
        processData: false,
        data:formData

    });
    ajaxRequest.done(function (xhr, textStatus) {
        console.log(xhr);
        if(xhr==1) {
            toastr.success("New Service Has Been Created Successfully", "Status")            
            $('#addservice')[0].reset();
        }else {
            toastr.error("Sorry An Error Occured Please Check and Try Again", "Status")
            console.log(xhr);
        }

    })
});

$('#changeservice').submit(function(e){
    e.preventDefault();

    var formData = new FormData($('#changeservice')[0]);
    var ajaxRequest = $.ajax({
        type: "POST",
        url: 'includes/manage_post.php',
        contentType: false,
        processData: false,
        data:formData

    });
    ajaxRequest.done(function (xhr, textStatus) {
        console.log(xhr);
        if(xhr==1) {
            toastr.success("Service Has Been Updated Successfully", "Status")            
            $('#changeservice')[0].reset();
        }else {
            toastr.error("Sorry An Error Occured Please Check and Try Again", "Status")
            console.log(xhr);
        }

    })
});
